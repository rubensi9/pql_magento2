//<![CDATA[
require([
    'jquery',
    'Magento_Ui/js/modal/alert'
], function ($, alert) {
    $('#product-shipping-method-button').on('click', function (event) {
        console.log('sendForm')
        let customurl = '<?php echo $block->getUrl('intelipost_quote/product/shipping') ?>'
        var form = $("#product-shipping-method");
        $.ajax({
            type: 'POST',
            async: false,
            url: customurl,
            data: form.serialize(),
            success: function (response) {
                console.log(response)
            },
            error: function (xhr, status, errorThrown) {
                console.log('Error happens. Try again.');
                console.log(status);
                console.log(errorThrown);
            }
        });
    })
}
);
    //]]>