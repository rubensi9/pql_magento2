
$(function(){

  $(".product-items").slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    centerMode: false,
    prevArrow: $("#arrow-prev"),
    nextArrow: $("#arrow-next"),
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
  
  $(".marcas.mobile").slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    centerMode: false,
	autoplay:true,
	autoplaySpeed:5000,
    prevArrow: $("#arrow-prev"),
    nextArrow: $("#arrow-next"),
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
  
  $(".categorias-banner").slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    centerMode: false,
	autoplay:true,
	autoplaySpeed:5000,
    prevArrow: $("#arrow-prev"),
    nextArrow: $("#arrow-next"),
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

})

