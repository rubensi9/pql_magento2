define([
    'jquery',
    'moment',
    'jquery/validate',
    'mage/translate'
], function ($, moment) {
    'use strict';

    $.validator.addMethod(
        'validate-dob',
        function (value) {
            if (value === '') {
                return true;
            }

            //return moment(value).isBefore(moment());
            return moment(value, ["DD.MM.YYYY"]).isBefore(moment());
        },
        $.mage.__('A data de nascimento não pode ser maior do que hoje.')
    );
});
