<?php
/*
 * @package     Intelipost_Quote
 * @copyright   Copyright (c) 2016 Gamuza Technologies (http://www.gamuza.com.br/)
 * @author      Eneias Ramos de Melo <eneias@gamuza.com.br>
 */

namespace Intelipost\Quote\Controller\Product;

class Shipping extends \Magento\Framework\App\Action\Action
{

    protected $_quote;
    protected $_coreSession;
    protected $_catalogSession;

    protected $_resultPageFactory;
    protected $resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Quote\Model\Quote $quote,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory

    )
    {
        $this->_quote = $quote;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_catalogSession = $catalogSession;
        $this->resultJsonFactory = $resultJsonFactory;

        parent::__construct ($context);
    }

    private function _getCatalogSession() 
    {
        return $this->_catalogSession;
    }

    public function execute()
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();   
        $logger = $objectManager->create('Psr\Log\LoggerInterface');
        $params = $this->getRequest()->getParams();
        $logger->info('params', $params);
        try {
            $country   = $this->getRequest()->getParam('country');
            $postcode  = $this->getRequest()->getParam('postcode');
            $productId = $this->getRequest()->getParam('product');
            $qty       = $this->getRequest()->getParam('qty');
            $url       = $this->getRequest()->getParam('url');

            $this->_quote->getShippingAddress()
                ->setCountryId($country)
                ->setPostcode($postcode)
                ->setCollectShippingRates(true);

            $product = $this->getProductById ($productId);

            $options = new \Magento\Framework\DataObject;
            $options->setProduct($product->getId());
            $options->setQty($qty);

            if (!strcmp ($product->getTypeId (), 'configurable'))
            {
                $superAttribute = $this->getRequest()->getParam('super_attribute');

                $options->setSuperAttribute ($superAttribute);
            }
            else if (!strcmp ($product->getTypeId (), 'bundle'))
            {
                $bundleOption    = $this->getRequest()->getParam('bundle_option');
                $bundleOptionQty = $this->getRequest()->getParam('bundle_option_qty');

                $options->setBundleOption ($bundleOption);
                $options->setBundleOptionQty ($bundleOptionQty);
            }

            $result = $this->_quote->addProduct($product, $options);
            if (empty($result)) die (__($result));

            $this->_quote->collectTotals();
            $result = $this->_quote->getShippingAddress()->getGroupedAllShippingRates();
            
        } catch (\Throwable $th) {
            echo '<pre>' . var_export($th->getMessage(), true) . '</pre>';
        }
        
        
        if (is_string ($result)) die ($result);
        $shippingresult = $result;
        $shippingReturns = array();
        foreach ($shippingresult as $code => $rates){
            $contRates = 0;
            foreach ($rates as $_rate){
                $shippingReturns[$code][$contRates]['method_title'] = $_rate->getMethodTitle();
                $shippingReturns[$code][$contRates]['method_price'] = $_rate->getPrice();
                $contRates++;
            }
        }
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($shippingReturns);

        /*  $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();        
        $catalogSession = $objectManager->get('\Magento\Catalog\Model\Session');
        $catalogSession->setShippingresult($result);
    $block->getCatalogSession()->setShippingResult($result);
    echo $block->getCatalogSession()->getMyName() . '<br />'; // output: Mageplaza
        $resultPage = $this->_resultPageFactory->create();
        $this->getResponse()->setBody(
            $resultPage->getLayout()
                ->createBlock('Magento\Framework\View\Element\Template')
                ->setRates($result)
                ->setTemplate('Intelipost_Quote::product/view/result.phtml')
                ->toHtml() 
        );*/
        $this->_redirect($url);
    }

    public function getProductById ($id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productManager = $objectManager->create('Magento\Catalog\Model\Product');

        return $productManager->load ($id);
    }

}

