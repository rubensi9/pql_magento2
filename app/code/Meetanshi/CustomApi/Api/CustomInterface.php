<?php
namespace Meetanshi\CustomApi\Api;
interface CustomInterface
{
    /**
    * * GET for Post api
    * @param int $orderid
    * @param string $urlboleto
    * @param string $urldanfe
    * @param string $urltracking
    * @return string
    */
    public function getPost($orderid, $urlboleto, $urldanfe, $urltracking);
}