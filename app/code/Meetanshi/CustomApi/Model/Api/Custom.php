<?php

namespace Meetanshi\CustomApi\Model\Api;

use Psr\Log\LoggerInterface;
use Magento\Framework\App\ResourceConnection;

class Custom
{
    protected $logger;
    public function __construct(LoggerInterface $logger, ResourceConnection $resourceConnection){
        $this->resourceConnection = $resourceConnection;
        $this->logger = $logger;
    }
    public function getPost($orderid, $urlboleto, $urldanfe, $urltracking){
        //{"order_id":359,"url_boleto":"http://boleto","url_danfe":"http://danfe"}
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();       
        $this->logger->info('----------------');
        $this->logger->info($orderid);
        $this->logger->info($urlboleto);
        $this->logger->info($urldanfe);
        $this->logger->info($urltracking);
        if($orderid){
            $connection  = $this->resourceConnection->getConnection();
            $order = $objectManager->create('Magento\Sales\Model\Order')->load($orderid);
    
            try {
                $order->setUrlBoleto($urlboleto);
                $order->setUrlDanfe($urldanfe);
                $order->setUrlTracking($urltracking);
                $order->save();
            } catch (\Exception $e) {
                $this->logger->info($e->getMessage());
            }
    
            $response = ['success' => false];
            try {
                $response = ['success' => true, 'orderId' => $orderid, 'urlboleto' => $urlboleto, 'urldanfe' => $urldanfe, 'urltracking' => $urltracking];
            }catch (\Exception $e) {
                $response = ['success' => false, 'message' => $e->getMessage()];
                $this->logger->info($e->getMessage());
            }
            $returnArray = json_encode($response);
            return $returnArray;
        } return null;
    }
}