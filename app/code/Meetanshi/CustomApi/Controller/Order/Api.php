<?php /**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Meetanshi\CustomApi\Controller\Order;

class Api extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $formKey;   
    protected $cart;
    protected $product;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(\Magento\Framework\App\Action\Context $context)
    {  
        parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $request = $objectManager->get('Magento\Framework\App\Request\Http');  
        $logger = $objectManager->create('Psr\Log\LoggerInterface');
        $logger->info('params',$params);
        $logger->info($request->getParam('order_id'));
        ///$values = json_decode($data, true);
        $orderId = $params['order_id'];
        $urlBoleto = $params['url_boleto'];
        $urlDanfe = $params['url_danfe'];
        $urlTracking = $params['url_tracking'];
        if(isset($params[2])){
            $order = $objectManager->create('Magento\Sales\Model\Order')->load($orderId);
    
            try {
                $order->setUrlBoleto($urlBoleto);
                $order->setUrlDanfe($urlDanfe);
                $order->setUrlTracking($urlTracking);
                $order->save();
            } catch (\Exception $e) {
                //$this->logger->info($e->getMessage());
            }
    
            $response = ['success' => false];
            try {
                $response = ['success' => true, 'orderId' => $orderId];
            }catch (\Exception $e) {
                $response = ['success' => false, 'message' => $e->getMessage()];
                //$this->logger->info($e->getMessage());
            }
            $returnArray = json_encode($response);
            return $returnArray;
        } return null;
    } 

}
