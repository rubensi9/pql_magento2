<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace MageHelp\PlaceOrder\Controller\Ajax;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * TaxVatCheck controller
 *
 */
class TaxVatCheck extends \Magento\Framework\App\Action\Action
{
    /** @var \Magento\Framework\Controller\Result\JsonFactory */
    protected $resultJsonFactory;

    /** @var \Magento\Framework\Controller\Result\RawFactory */
    protected $resultRawFactory;

    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    /**  @var ScopeConfigInterface */
    protected $customerCollection;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $_storeManager;

    /** @var \Psr\Log\LoggerInterface */
    protected $_logger;

    /**
     * Initialize Login controller
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magento\Customer\Model\ResourceModel\Customer\Collection $customerCollection
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        \Magento\Customer\Model\ResourceModel\Customer\Collection $customerCollection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->customerCollection = $customerCollection;
        $this->_storeManager = $storeManager;
        $this->_logger = $logger;
    }

    /**
     * Login registered users and initiate a session.
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $httpBadRequestCode = 400;

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();

        if ($this->getRequest()->getMethod() !== 'POST' || !$this->getRequest()->isXmlHttpRequest()) {
            return $resultRaw->setHttpResponseCode($httpBadRequestCode);
        }

        try {

            $taxvat = $this->getRequest()->getPostValue();
            $taxvatOnlyNumbers = preg_replace('/\D/', '', $taxvat);

            $collection = $this->customerCollection->addAttributeToSelect('entity_id')
            ->addAttributeToFilter([
                ['attribute'=>'taxvat','eq'=>$taxvat],
                ['attribute'=>'taxvat','eq'=>$taxvatOnlyNumbers]
            ])
            ->addAttributeToFilter('store_id',$this->_storeManager->getStore()->getId());
            if($collection->getSize()){
                $response = false;
            }else{
                $response = true;
            }

        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
            $response = false;
        }

        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($response);
    }
}
