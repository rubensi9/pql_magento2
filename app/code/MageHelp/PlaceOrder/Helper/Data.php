<?php

namespace MageHelp\PlaceOrder\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $productRepository;
    protected $priceHelper;
    protected $storeInfo;
    protected $storeManager;
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Framework\Pricing\Helper\Data
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Pricing\Helper\Data $price_helper,
        \Magento\Store\Model\Information $storeInfo,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ){
        $this->productRepository = $productRepository;
        $this->priceHelper = $price_helper;
        $this->storeInfo = $storeInfo;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }
    /**
     * @param string $price
     * @return float|string
     */
    public function getFormatedPrice($price=''){
        return $this->priceHelper->currency($price, true, false);
    }
    /**
     * @param $id
     * @return \Magento\Catalog\Api\Data\ProductInterface|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductById($id) {
        return $this->productRepository->getById($id);
    }

    public function getPhoneNumber() {
        return $this->storeInfo->getStoreInformationObject($this->storeManager->getStore())->getPhone();
    }
}

