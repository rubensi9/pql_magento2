<?php

namespace MageHelp\PlaceOrder\Api;

interface ConsultaInterface
{
    /**
     * Search Cep
     *
     * @param string $cep
     * @return \MageHelp\PlaceOrder\Api\Data\Data|Bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function consultaCep($cep);

}
