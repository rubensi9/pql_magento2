define([
    'Magento_Ui/js/form/element/abstract',
    'jquery',
    'mask'
], function (Abstract, jquery, mask) {
    'use strict';

    return Abstract.extend({

        initialize: function () {
            this._super();
            jquery('#'+this.uid).mask('(00) 0000-00009');
            return this;
        }

    });
});

