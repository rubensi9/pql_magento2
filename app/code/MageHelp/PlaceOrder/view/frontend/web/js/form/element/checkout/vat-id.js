define([
    'jquery',
    'Magento_Ui/js/lib/validation/validator',
    'Magento_Ui/js/form/element/abstract',
    'pcustomer',
], function($, validator, Element, pcustomer) {
    'use strict';

    return Element.extend({
        initialize: function() {

            validator.addRule(
                'validate-taxvat-br',
                function(value) {
                    return pcustomer.validateCPF(value,0);
                },
                $.mage.__('Informe um CPF válido.')
            );

            validator.addRule(
                'validate-exists',
                function(value) {
                    console.log('validate-exists');
                    return pcustomer.cpfcnpjExists(value, '/registervalidate/ajax/taxvatcheck');
                },
                $.mage.__('CPF já cadastrado.')
            );

            this._super();
        },

    });

});