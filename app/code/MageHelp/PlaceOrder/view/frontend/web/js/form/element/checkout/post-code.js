define([
    'underscore',
    'ko',
    'uiRegistry',
    'Magento_Ui/js/form/element/abstract',
    'jquery',
    'mask',
    'mage/url',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-rate-processor/new-address',
    'Magento_Checkout/js/model/shipping-rate-processor/customer-address',
    'Magento_Checkout/js/model/shipping-rate-registry'
], function (_, ko, registry, Abstract, jquery, mask, url, quote, defaultProcessor, customerAddressProcessor, rateRegistry) {
    'use strict';

    return Abstract.extend({
        defaults: {
            loading: ko.observable(false),
            imports: {
                update: '${ $.parentName }.country_id:value'
            }
        },

        initialize: function () {
            this._super();
            jquery('#'+this.uid).mask('00000-000');
            return this;
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            var country = registry.get(this.parentName + '.' + 'country_id'),
                options = country.indexedOptions,
                option;

            if (!value) {
                return;
            }

            if(options[value]){
                option = options[value];

                if (option['is_zipcode_optional']) {
                    this.error(false);
                    this.validation = _.omit(this.validation, 'required-entry');
                } else {
                    this.validation['required-entry'] = true;
                }

                this.required(!option['is_zipcode_optional']);

            }

            this.firstLoad = true;
        },


        onUpdate: function () {

            var element = this;
            if(this.value() && this.value().length == 9) {
                this.bubble('update', this.hasChanged());
                var validate = this.validate();

                if (validate.valid == true) {

                    var value = this.value();
                    value = value.replace('-', '');
                    var ajaxurl = url.build("/rest/V1/consultaCep/" + value);

                    jquery.ajax({
                        url: ajaxurl,
                        dataType: 'json',
                        async: false,
                        success: function (data) {
                            if (data.error) {
                                console.log(data.error);
                            } else {

                                var processors = [];
                                var address = quote.shippingAddress();
                                if(!address['postcode']){
                                    address['postcode'] = value;
                                }

                                rateRegistry.set(address.getKey(), null);
                                rateRegistry.set(address.getCacheKey(), null);
                                processors.default = defaultProcessor;
                                processors['customer-address'] = customerAddressProcessor;
                                var type = address.getType();
                                if (processors[type]) {
                                    processors[type].getRates(address);
                                } else {
                                    defaultProcessor.getRates(address);
                                }

console.log(element.parentName);
                                if (registry.get(element.parentName + '.' + 'country_id')) {
                                    registry.get(element.parentName + '.' + 'country_id').value('BR');
                                }
                                if (registry.get(element.parentName + '.' + 'street.0')) {
                                    registry.get(element.parentName + '.' + 'street.0').value(data.logradouro);
                                }
                                if (registry.get(element.parentName + '.' + 'street.3')) {
                                    registry.get(element.parentName + '.' + 'street.3').value(data.bairro);
                                }
                                if (registry.get(element.parentName + '.' + 'city')) {
                                    registry.get(element.parentName + '.' + 'city').value(data.cidade);
                                }
                                if (registry.get(element.parentName + '.' + 'region_id')) {
                                    registry.get(element.parentName + '.' + 'region_id').value(data.uf);
                                }
                            }
                        }
                    });
                }
            }else{

                var address = quote.shippingAddress();
                // Limpar dados caso cep incompleto
                if(rateRegistry.get(address.getCacheKey()).length){

                    address['postcode'] = null;

                    var processors = [];
                    rateRegistry.set(address.getKey(), null);
                    rateRegistry.set(address.getCacheKey(), null);
                    processors.default = defaultProcessor;
                    processors['customer-address'] = customerAddressProcessor;
                    var type = address.getType();
                    if (processors[type]) {
                        processors[type].getRates(address);
                    } else {
                        defaultProcessor.getRates(address);
                    }

                    registry.get(element.parentName + '.' + 'street.0').value('');
                    registry.get(element.parentName + '.' + 'street.1').value('');
                    registry.get(element.parentName + '.' + 'street.2').value('');
                    registry.get(element.parentName + '.' + 'street.3').value('');
                    registry.get(element.parentName + '.' + 'city').value('');
                    registry.get(element.parentName + '.' + 'region_id').value('');
                }
            }
        }
    });
});
