define([
    "jquery",
    'mage/url',
    'mask'
], function($, mageurl, mask) {
    "use strict";

    return {
        init: function(){
            $('#taxvat').mask('000.000.000-00');
            $('#vat_id').mask('000.000.000-00');
            // var setupPF = function(){
            //     $('#taxvat').attr('maxlength','14');
            //     $('#ie_rg').attr('maxlength','20');
            //     $('label[for="firstname"]').html('<span>Nome</span>');
            //     $('label[for="taxvat"]').html('<span>CPF</span>');
            //     $('label[for="ie_rg"]').html('<span>RG</span>');
            //     $('#firstname').first().attr('title','Nome');
            //     $('.field-name-lastname').show();
            //     $('#lastname').val('');
            //     $('.gender').show();
            //     $('.field-dob').show();
            //     return true;
            // };
            // var setupPJ = function(){
            //     $('#taxvat').attr('maxlength','18');
            //     $('#ie_rg').attr('maxlength','20');
            //     $('label[for="firstname"]').html('<span>Razão social</span>');
            //     $('#firstname').first().attr('title','Razão social');
            //     $('.field-name-lastname').hide();
            //     $('#lastname').val("-");
            //     $('label[for="taxvat"]').html('<span>CNPJ</span>');
            //     $('label[for="ie_rg"]').html('<span>Incrição Estadual</span>');
            //     $('.gender').hide();
            //     $('.field-dob').hide();
            //     return true;
            // };
            //
            // setupPF();
            //
            // $('#type_person').on('click', function(){
            //     var selectedOption = $(this).find('option:selected').text();
            //     if(selectedOption == 'Física'){
            //         setupPF();
            //     }else if(selectedOption == 'Jurídica') {
            //         setupPJ();
            //     }
            // });

            // $('#taxvat').on('keyup', function() {
            //     var v = $(this).val().replace(/\D/g, "");
            //     if (v.length <= 11) {
            //         v = v.replace(/(\d{3})(\d)/, "$1.$2");
            //         v = v.replace(/(\d{3})(\d)/, "$1.$2");
            //         v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            //     } else {
            //         v = v.replace(/^(\d{2})(\d)/, "$1.$2");
            //         v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
            //         v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
            //         v = v.replace(/(\d{4})(\d)/, "$1-$2");
            //     }
            //     $(this).val(v);
            // });

            $('#dob').on('keyup', function() {
                $(this).attr('maxlength','10');
                var v = $(this).val().replace(/\D/g, "");
                v = v.replace(/(\d{2})(\d)/, "$1/$2");
                v = v.replace(/(\d{2})(\d)/, "$1/$2");
                $(this).val(v);
            });

            $('#zip').on('keyup', function() {
                $(this).attr('maxlength','9');
                var v = $(this).val().replace(/\D/g, "");
                v = v.replace(/^(\d{5})(\d)/,"$1-$2");
                $(this).val(v);
            });

            $('#zip').on('change', function() {
                let value = $(this).val();

                if(value && value.length == 9) {
                    value = value.replace('-', '');
                    let ajaxurl = mageurl.build("/rest/V1/consultaCep/" + value);

                    $.ajax({
                        url: ajaxurl,
                        dataType: 'json',
                        async: false,
                        success: function (data) {
                            if (data.error) {
                                console.log(data.error);
                            } else {
                                $('#street_1').val(data.logradouro);
                                $('#street_4').val(data.bairro);
                                $('#city').val(data.cidade);
                                $('#region_id').val(data.uf);
                            }
                        }
                    });
                }else{
                    $('#street_1').val('');
                    $('#street_4').val('');
                    $('#city').val('');
                    $('#region_id').val('');
                }
            });

            $('#telephone').on('keyup', function() {
                $(this).attr('maxlength','16');
                var v = $(this).val();
                v = v.replace(/\D/g, "");
                v = v.replace(/^(\d{2})(\d)/g, "($1) $2");
                v = v.replace(/(\d)(\d{4})$/, "$1-$2");
                $(this).val(v);
            });

            $('#fax').on('keyup', function() {
                $(this).attr('maxlength','16');
                var v = $(this).val();
                v = v.replace(/\D/g, "");
                v = v.replace(/^(\d{2})(\d)/g, "($1) $2");
                v = v.replace(/(\d)(\d{4})$/, "$1-$2");
                $(this).val(v);
            });

        },
        validateCPF: function (cpf, pType) {
            var cpf_filtrado = "", valor_1 = " ", valor_2 = " ", ch = "";
            var valido = false;
            var i = 0;
            for (i; i < cpf.length; i++) {
                ch = cpf.substring(i, i + 1);
                if (ch >= "0" && ch <= "9") {
                    cpf_filtrado = cpf_filtrado.toString() + ch.toString();
                    valor_1 = valor_2;
                    valor_2 = ch;
                }
                if ((valor_1 != " ") && (!valido))
                    valido = !(valor_1 == valor_2);
            }
            if (!valido)
                cpf_filtrado = "12345678912";
            if (cpf_filtrado.length < 11) {
                for (i = 1; i <= (11 - cpf_filtrado.length); i++) {
                    cpf_filtrado = "0" + cpf_filtrado;
                }
            }
            if (pType <= 1) {
                if ((cpf_filtrado.substring(9, 11) == this.checkCPF(cpf_filtrado.substring(0, 9))) && (cpf_filtrado.substring(11, 12) == "")) {
                    return true;
                }
            }
            if ((pType == 2) || (pType == 0)) {
                if (cpf_filtrado.length >= 14) {
                    if (cpf_filtrado.substring(12, 14) == this.checkCNPJ(cpf_filtrado.substring(0, 12))) {
                        return true;
                    }
                }
            }
            return false;
        },
        checkCNPJ: function(vCNPJ) {
            var mControle = "";
            var mControle1 = "";
            var mDigito = "";
            var aTabCNPJ = new Array(5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2);
            for (let i = 1; i <= 2; i++) {
                var mSoma = 0;
                var j = 0;
                for (j; j < vCNPJ.length; j++)
                    mSoma = mSoma + (vCNPJ.substring(j, j + 1) * aTabCNPJ[j]);
                if (i == 2)
                    mSoma = mSoma + (2 * mDigito);
                mDigito = (mSoma * 10) % 11;
                if (mDigito == 10)
                    mDigito = 0;
                mControle1 = mControle;
                mControle = mDigito;
                aTabCNPJ = new Array(6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3);
            }
            return((mControle1 * 10) + mControle);
        },
        checkCPF: function(vCPF) {
            var mControle = "";
            var mControle1 = "";
            var mDigito = "";
            var mContIni = 2, mContFim = 10, mDigito = 0;
            var j = 1;
            for (j; j <= 2; j++) {
                var mSoma = 0;
                var i = mContIni;
                for (i; i <= mContFim; i++)
                    mSoma = mSoma + (vCPF.substring((i - j - 1), (i - j)) * (mContFim + 1 + j - i));
                if (j == 2)
                    mSoma = mSoma + (2 * mDigito);
                mDigito = (mSoma * 10) % 11;
                if (mDigito == 10)
                    mDigito = 0;
                mControle1 = mControle;
                mControle = mDigito;
                mContIni = 3;
                mContFim = 11;
            }

            return((mControle1 * 10) + mControle);
        },
        cpfcnpjExists: function(value, url) {
            var response = false;
            $.ajax({
                showLoader: true,
                url: mageurl.build(url),
                async : false,
                data: 'taxvat=' + encodeURIComponent(value),
                type: "POST",
                dataType: 'json'
            }).done(function (data) {
                response = data;
            });
            return response;
        }
    }

});
