/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'mage/storage',
    'Magento_Checkout/js/model/url-builder'
], function ($, storage, urlBuilder) {
    'use strict';

    return function (deferred, email) {
        return storage.post(
            urlBuilder.createUrl('/customers/isEmailAvailable', {}),
            JSON.stringify({
                customerEmail: email
            }),
            false
        ).done(function (isEmailAvailable) {
            if (isEmailAvailable) {
                $('.opc-wrapper .form-shipping-address').show('slow');
                deferred.resolve();
            } else {
                $('.opc-wrapper .form-shipping-address').hide('slow');
                deferred.reject();
            }
        }).fail(function () {
            deferred.reject();
        });
    };
});
