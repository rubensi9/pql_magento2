<?php
namespace MageHelp\PlaceOrder\Model;

use MageHelp\PlaceOrder\Api\ConsultaInterface;
use MageHelp\PlaceOrder\Api\Data\Data;
use MageHelp\PlaceOrder\Model\Address\Viacep;
use MageHelp\PlaceOrder\Model\Address\Local;

/**
 * Class Search
 * @package MageHelp\PlaceOrder\Model
 */
class Consulta extends \Magento\Framework\DataObject implements Data, ConsultaInterface
{
    /**
     * @var \Magento\Directory\Model\Region
     */
    protected $_modelRegion;

    /**
     * Search constructor.
     * @param \Magento\Directory\Model\Region $modelRegion
     * @param array $data
     */
    public function __construct(\Magento\Directory\Model\Region $modelRegion,
                                \Magento\Quote\Model\Quote $quote,
                                \Magento\Framework\App\Request\Http $request,
                                \Magento\Quote\Model\Quote\TotalsCollector $totalsCollector,
                                \Magento\Framework\Pricing\Helper\Data $priceHelper,
                                array $data = []
    )
    {
        $this->_modelRegion = $modelRegion;
        parent::__construct($data);
    }

    /**
     * Search cep
     * @param int $cep
     * @return $this|bool
     */
    public function consultaCep($cep){

        //Busca local
        /*
        $data = Local::getEndereco($cep);
        if($data){
            $this->setData('logradouro', $data['logradouro']);
            $this->setData('bairro', $data['bairro']);
            $this->setData('cep', $data['cep']);
            $this->setData('cidade', $data['cidade']);
            $this->setData('uf', $this->getRegionId($data['uf']));
            return $this;
        }
        */

        //Busca online
        $data = Viacep::getEndereco($cep);
        if($data){
            $this->setData('logradouro', $data['logradouro']);
            $this->setData('bairro', $data['bairro']);
            $this->setData('cep', $data['cep']);
            $this->setData('cidade', $data['cidade']);
            $this->setData('uf', $this->getRegionId($data['uf']));
            return $this;
        }

        return false;
    }

    /**
     * @param string $uf
     * @param string $country_id
     * @return mixed
     */
    public function getRegionId($uf, $country_id = "BR"){
        return $this->_modelRegion->loadByCode($uf, $country_id)->getId();
    }


    /**
     * @return string
     */
    public function getLogradouro()
    {
        return $this->getData('logradouro');
    }

    /**
     * @return string
     */
    public function getBairro()
    {
        return $this->getData('bairro');
    }

    /**
     * @return string
     */
    public function getCep()
    {
        return $this->getData('cep');
    }

    /**
     * @return string
     */
    public function getCidade()
    {
        return $this->getData('cidade');
    }

    /**
     * @return string
     */
    public function getUf()
    {
        return $this->getData('uf');
    }

    public function getError()
    {
        return $this->getData('error');
    }

}