<?php
namespace MageHelp\PlaceOrder\Model\Address;
/**
 * Class Viacep
 * Traz endereço do cep usando webservice via cep
 * @package MageHelp\PlaceOrder\Model\Endereco
 */
class Viacep
{

	/**
	 * @param string $cep
	 * @return array|bool
	 */
	public static function getEndereco($cep){

		$html = self::_request('https://viacep.com.br/ws/'.$cep.'/json/');
		$json = json_decode($html, 1);
		
		if($json){
			$dados = array(
				'logradouro' => isset($json['logradouro']) ? $json['logradouro'] : '',
				'bairro' => isset($json['bairro']) ? $json['bairro'] : '',
				'cep' => (int)$cep,
				'cidade' => isset($json['localidade']) ? $json['localidade'] : '',
				'uf' => isset($json['uf']) ? strtoupper($json['uf']) : ''
			);
			
			if(strpos($dados['logradouro'], ' - ') !== false){
				$l = explode(' - ', $dados['logradouro']);
				$dados['logradouro'] = $l[0];
			}
			
			return $dados;
		}
		
		return false;
	}

	/**
	 * @param string $url
	 * @param array $get
	 * @return mixed
	 */
	public static function _request($url, $get=array())
	{
		$ch = curl_init($url);
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		return curl_exec($ch);
	}
	
}
