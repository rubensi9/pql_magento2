<?php /**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace ClassyLlama\LlamaCoin\Controller\Customer;

class Taxvat extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $formKey;   
    protected $cart;
    protected $product;

    protected $request;
    protected $_customerFactory;
    protected $resultJsonFactory;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    )
    {  
        $this->request = $request;
        $this->_customerFactory = $customerFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Get customer collection
     */
    public function getCustomerCollection()
    {
        return $this->_customerFactory->create();
    }

    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {

        $params = $this->getRequest()->getParams();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('Psr\Log\LoggerInterface');
        $cpf = $params['cpf'];
        $customerCollection = $this->getCustomerCollection();
        $resultJson = $this->resultJsonFactory->create();
        foreach ($customerCollection as $customer) {
            if($customer->getTaxvat() == $cpf){
                return $resultJson->setData(['success' => false]);
            } 
        }
        return $resultJson->setData(['success' => true]);
    } 

}
