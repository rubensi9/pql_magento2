<?php /**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace ClassyLlama\LlamaCoin\Controller\Cart;

class Paymentdata extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $formKey;   
    protected $cart;
    protected $product;

    protected $request;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\RequestInterface $request
    )
    {  
        $this->request = $request;
        parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "http://pqlbrasil.com.br/index.php/rest/V1/integration/admin/token?username=tagweb&password=tag.890",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Postman-Token: b7d053fe-df0d-4a85-ab82-4093a9be9d15"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            var_dump(__LINE__);
            echo "cURL Error #:" . $err;
        } else {
            var_dump(__LINE__);
            $adminToken = $response;
        }

        $curl = curl_init();
        var_dump($adminToken);

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://pqlbrasil.com.br/index.php/rest/V1/orders",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".$adminToken,
                "Cache-Control: no-cache",
                "Postman-Token: 1057fee5-abde-4a88-bd39-2a623ca995e6"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            var_dump(__LINE__);
            echo "cURL Error #:" . $err;
        } else {
            var_dump(__LINE__);
            echo $response;
        }
        die(__METHOD__);
    } 

}
