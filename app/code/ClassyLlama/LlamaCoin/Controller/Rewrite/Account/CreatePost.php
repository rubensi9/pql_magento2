<?php
/**
 * Hello Rewrite Account Create Controller
 *
 * @category    Webkul
 * @package     Webkul_Hello
 * @author      Webkul Software Private Limited
 *
 */
namespace ClassyLlama\LlamaCoin\Controller\Rewrite\Account;

class CreatePost extends \Magento\Customer\Controller\Account\CreatePost
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        // Do your stuff here
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $subscriberFactory = $objectManager->create('Magento\Newsletter\Model\SubscriberFactory');
        $params = $this->getRequest()->getParams();
        //var_dump($arams);
        if($params['newsletter_confirmation']){
            $subscriberFactory->create()->subscribe($params['email']);
        }
        //die(__METHOD__);
        return parent::execute();
    }
}