<?php
namespace ClassyLlama\LlamaCoin\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();   
       
        $table = $setup->getConnection()
            ->newTable($setup->getTable('classyllama_llamacoin_paymentdata'))
            ->addColumn(
                'paymentdata_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'paymentdata_id'
            )
            ->addColumn(
                'quote_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'quote_id'
            )
            ->addColumn(
                'nomeimpresso',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                    'nomeimpresso'
            )
            ->addColumn(
                'cpf',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                    'cpf'
            )
            ->addColumn(
                'parcelas',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                    'parcelas'
            )
            ->setComment("Greeting Message table");
        $setup->getConnection()->createTable($table);
        $setup->endSetup();
    }
}
 