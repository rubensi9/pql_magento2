<?php
namespace ClassyLlama\LlamaCoin\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class StatusChange implements ObserverInterface
{
    protected $transportBuilder;
    protected $storeManager;
    protected $inlineTranslation;
    protected $scopeConfig;

    public function __construct(
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        StateInterface $state
    )
    {
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->inlineTranslation = $state;
    }

    public function sendMail($order)
    {
        $orderData = $order->getData();
        $toEmail = $order->getCustomerEmail();
        $toName = $order->getCustomerName();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('Psr\Log\LoggerInterface');
        
        $fromEmail = $this->scopeConfig->getValue('trans_email/ident_support/email',ScopeInterface::SCOPE_STORE);
        $fromName  = $this->scopeConfig->getValue('trans_email/ident_support/name',ScopeInterface::SCOPE_STORE);
        $telephone  = $this->scopeConfig->getValue('general/store_information/phone',ScopeInterface::SCOPE_STORE);
        // this is an example and you can change template id,fromEmail,toEmail,etc as per your need.
        $templateId = 11; // template id
        //$fromEmail = 'no-reply@ciaeletronicos.com.br';  // sender Email id
        //$fromName = 'Admin';             // sender Name
        //$toEmail = $orderData['customer_email']; // receiver email id
        // template variables pass here
        //customer_email
        $logger->info('orderData',$orderData);
        $templateVars = [
            'order_data' => $orderData,
            'store_phone' => $telephone,
            'store_name' => 'PQL',
            'store_email' => 'contato@putzquelegal.com.br'
        ];

        $storeId = $this->storeManager->getStore()->getId();
        
        $from = ['email' => $fromEmail, 'name' => $fromName];
        $to = ['email' => $toEmail, 'name' => $toName];
        $this->inlineTranslation->suspend();
        //c0a681261eafe408ecf714036bcee4e16b4fbde8
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $state =  $objectManager->get('Magento\Framework\App\State');
        $templateOptions = [
            'area' => $state->getAreaCode(),
            'store' => $storeId
        ];
        $transport = $this->transportBuilder->setTemplateIdentifier($templateId, $storeScope)
            ->setTemplateOptions($templateOptions)
            ->setTemplateVars($templateVars)
            ->setFrom($from)
            ->addTo($to)
            ->getTransport();
        try {
            $transport->sendMessage();
            $this->inlineTranslation->resume();
        } catch (\Exception $e) {
            $logger->info($e->getMessage());
        }
    }

    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        
        if ($order instanceof \Magento\Framework\Model\AbstractModel) {
            $this->sendMail($order);/* 
            switch($order->getStatus()){
                case 'aguardando_pagamento': 
                
            } */
        }

    }
}