<?php
namespace ClassyLlama\LlamaCoin\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\AlreadyExistsException;

class Newsletterinclude implements ObserverInterface
{
    protected $_customerCollectionFactory;
    protected $subscriberFactory;

    public function __construct(
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory
    ) {
        $this->_customerCollectionFactory = $customerCollectionFactory;
        $this->subscriberFactory= $subscriberFactory;
    }

    public function execute(Observer $observer)
    {
        $customer = $observer->getCustomer();
        var_dump($customer->getData('email'));
        var_dump($customer->getData('email'));
        $customerCollection= $this->_customerCollectionFactory->create();
        $this->subscriberFactory->create()->subscribe($customer->getData('email'));

        $taxvat = $customer->getData('taxvat');
        if(!$this->_validaCPF($taxvat)){
            throw new AlreadyExistsException(
                __('CPF Inválido')
        }
    }
}