<?php
namespace ClassyLlama\LlamaCoin\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class SalesOrderPlaceAfter implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $order = $observer->getOrder();
        $payment = $order->getPayment();
        
        $lastTransId = $payment->getLastTransId();
        
        if ($lastTransId > 0) {
            $order->setState('payment_review');
            $order->setStatus('payed');
            $order->save();
        }
    }
}