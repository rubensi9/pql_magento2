define([
        'jquery',
        'Magento_Payment/js/view/payment/cc-form'
    ],
    function ($, Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'ClassyLlama_LlamaCoin/payment/llamacoin',
            },

            context: function() {
                return this;
            },

            getCode: function() {
                return 'classyllama_llamacoin';
            },
            
            getData: function () {
                var data = {
                    'method': 'classyllama_llamacoin',
                    'additional_data': {
                        'nome_impresso': jQuery('[name="payment[nomeimpresso]"]').val(),
                        'cpf': jQuery('[name="payment[cpf]"]').val(),
                        'parcelas': jQuery('[name="payment[parcelas]"]').val(),
                        'cc_cid': this.creditCardVerificationNumber(),
                        'cc_ss_start_month': this.creditCardSsStartMonth(),
                        'cc_ss_start_year': this.creditCardSsStartYear(),
                        'cc_ss_issue': this.creditCardSsIssue(),
                        'cc_type': this.creditCardType(),
                        'cc_exp_year': this.creditCardExpYear(),
                        'cc_exp_month': this.creditCardExpMonth(),
                        'cc_number': this.creditCardNumber()
                    },
                };
                
                return data;
            },

            isActive: function() {
                return true;
            },
        });
        
    }
);