<?php
namespace ClassyLlama\LlamaCoin\Model;

class Paymentdata extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'classyllama_llamacoin_paymentdata';

	protected $_cacheTag = 'classyllama_llamacoin_paymentdata';

	protected $_eventPrefix = 'classyllama_llamacoin_paymentdata';

	protected function _construct()
	{
		$this->_init('ClassyLlama\LlamaCoin\Model\ResourceModel\Paymentdata');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}