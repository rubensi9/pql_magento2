<?php
namespace ClassyLlama\LlamaCoin\Model\ResourceModel;

class Paymentdata extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('classyllama_llamacoin_paymentdata', 'paymentdata_id');
	}
	
}