<?php
namespace ClassyLlama\LlamaCoin\Model\ResourceModel\Post;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'paymentdata_id';
	protected $_eventPrefix = 'classyllama_llamacoin_paymentdata_collection';
	protected $_eventObject = 'paymentdata_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('ClassyLlama\LlamaCoin\Model\Paymentdata', 'ClassyLlama\LlamaCoin\Model\ResourceModel\Paymentdata');
	}

}