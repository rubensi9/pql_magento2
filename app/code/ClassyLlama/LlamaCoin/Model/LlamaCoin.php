<?php

namespace ClassyLlama\LlamaCoin\Model;

class LlamaCoin extends \Magento\Payment\Model\Method\Cc
{
    const CODE = 'classyllama_llamacoin';

    protected $_code = self::CODE;

    protected $_canAuthorize = true;
    protected $_canCapture = true;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $moduleList,
            $localeDate,
            $resource,
            $resourceCollection
        );
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $this->_transportBuilder = $objectManager->get('Magento\Framework\Mail\Template\TransportBuilder');
        $this->_inlineTranslation = $objectManager->get('Magento\Framework\Translate\Inline\StateInterface');
        $this->_storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
    }

    /**
     * Capture Payment.
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this
     */
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        try {
            
            //check if payment has been authorized
            if(is_null($payment->getParentTransactionId())) {
                $this->authorize($payment, $amount);
            }

            //build array of payment data for API request.
            $request = [
                'capture_amount' => $amount,
                //any other fields, api key, etc.
            ];
            
            //make API request to credit card processor.
            $response = $this->makeCaptureRequest($request);

            //todo handle response

            //transaction is done.
            $payment->setIsTransactionClosed(1);

        } catch (\Exception $e) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $logger = $objectManager->create('Psr\Log\LoggerInterface');
        }

        return $this;
    }
    
    public function assignData(\Magento\Framework\DataObject $data)
    {
        parent::assignData($data);
        $this->getInfoInstance()->setAdditionalInformation('post_data_value', $data->getData());
    
        return $this;
    }

    /**
     * Authorize a payment.
     *
     * @param \Magento\Payment\Model\InfoInterface $payment
     * @param float $amount
     * @return $this
     */
    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('Psr\Log\LoggerInterface');
        try {
            
            $info = $this->getInfoInstance();
            $additionalInformation = $info->getAdditionalInformation()['post_data_value']['additional_data'];
            
            ///build array of payment data for API request.
            $request = [
                'cc_type' => $payment->getCcType(),
                'cc_exp_month' => $payment->getCcExpMonth(),
                'cc_exp_year' => $payment->getCcExpYear(),
                'cc_number' => $payment->getCcNumber(),
                'cc_cid' => $payment->getCcCid(),
                'nome_impresso' => $additionalInformation['nome_impresso'],
                'cpf' => $additionalInformation['cpf'],
                'parcelas' => $additionalInformation['parcelas'],
                'amount' => $amount,
                'order' => $payment->getOrder(),
                //'order_id' => $payment->getParentId()
            ];

            //https://stackoverflow.com/questions/8842978/get-current-orderid-in-magento-payment-module-during-checkout

            
            //check if payment has been authorized
            $response = $this->makeAuthRequest($request);
            
            //if (!empty($response) && !empty($response['order'])) {
                //$payment->setOrder($response['order']);
            //}
        } catch (\Exception $e) {
            
            $logger->info($e->getMessage());
            //$this->debug($payment->getData(), $e->getMessage());
        }

        if(isset($response['transactionID'])) {
            // Successful auth request.
            // Set the transaction id on the payment so the capture request knows auth has happened.
            $payment->setTransactionId($response['transactionID']);
            $payment->setParentTransactionId($response['transactionID']);
        }

        //processing is not done yet.
        $payment->setIsTransactionClosed(0);

        return $this;
    }

    /**
     * Set the payment action to authorize_and_capture
     *
     * @return string
     */
    public function getConfigPaymentAction()
    {
        return self::ACTION_AUTHORIZE_CAPTURE;
    }

    /**
     * Test method to handle an API call for authorization request.
     *
     * @param $request
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function makeAuthRequest($request)
    {
        //$response = ['transactionId' => 123]; //todo implement API call for auth request.
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('Psr\Log\LoggerInterface');
        $jsonDecoded = $this->callApi($request);
        $resultCode = $jsonDecoded['ResultCode']; 
        $response = null;
        $logger->info('DEBUG PINBANK', $jsonDecoded);
        if($resultCode == 0){
            $response = ['transactionID' => 123];
            $this->_sendSuccessEmail();
            
        } else{
            //$message = $jsonDecoded['Message'];
            //$toDecrypt = $jsonDecoded['Data']['Json'];
            
            $this->_sendFailedEmail();
        }

        if($response == null) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Failed auth request.'));
        }

        return $response;
    }

    private function _sendFailedEmail(){
        $om =  \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $om->get('Magento\Customer\Model\Session');
        
        if($customerSession->isLoggedIn()) {
            
            $toEmail =  $customerSession->getCustomer()->getEmail();
            $toEmail = 'tetse@mimpi99.com';
            
            $firstname =  $customerSession->getCustomer()->getFirstname();
            $lastname =  $customerSession->getCustomer()->getLastname();
            $templateId = 10; // template id
            $fromEmail = 'no-reply@domain.com';  // sender Email id
            $fromName = 'PQL'; // sender Name
     
            try {
                // template variables pass here
                $templateVars = [
                    'name' => $firstname.' '.$lastname,
                    'msg1' => 'test1'
                ];
     
                $storeId = $this->_storeManager->getStore()->getId();
     
                $from = ['email' => $fromEmail, 'name' => $fromName];
                $this->_inlineTranslation->suspend();
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                
                $templateOptions = [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $storeId
                ];
                
                $transport = $this->_transportBuilder->setTemplateIdentifier($templateId, $storeScope)
                    ->setTemplateOptions($templateOptions)
                    ->setTemplateVars($templateVars)
                    ->setFrom($from)
                    ->addTo($toEmail)
                    ->getTransport();
                    
                $transport->sendMessage();
                $this->inlineTranslation->resume();
            } catch (\Exception $e) {
                $this->_logger->info($e->getMessage());
            }
        }
    }

    private function _sendSuccessEmail(){
        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $om->get('Magento\Customer\Model\Session');
        
        if($customerSession->isLoggedIn()) {
            
            $toEmail = $customerSession->getCustomer()->getEmail();
            $toEmail = 'tetse@mimpi99.com';

            $firstname = $customerSession->getCustomer()->getFirstname();
            $lastname = $customerSession->getCustomer()->getLastname();
            $templateId = 9; // template id
            $fromEmail = 'no-reply@domain.com'; // sender Email id
            $fromName = 'PQL'; // sender Name
            
            try {
                // template variables pass here
                $templateVars = [
                    'name' => $firstname.' '.$lastname,
                    'msg1' => 'test1'
                ];
                
                $storeId = $this->_storeManager->getStore()->getId();
                $from = ['email' => $fromEmail, 'name' => $fromName];
                $this->_inlineTranslation->suspend();
                $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                
                $templateOptions = [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $storeId
                ];
                
                $transport = $this->_transportBuilder->setTemplateIdentifier($templateId, $storeScope)
                    ->setTemplateOptions($templateOptions)
                    ->setTemplateVars($templateVars)
                    ->setFrom($from)
                    ->addTo($toEmail)
                    ->getTransport();
                
                $transport->sendMessage();
                $this->_inlineTranslation->resume();
            } catch (\Exception $e) {
                $this->_logger->info($e->getMessage());
            }
        }
    }
    /**
     * Test method to handle an API call for capture request.
     *
     * @param $request
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function makeCaptureRequest($request)
    {
        $response = ['success'];
        if($response == null) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Failed capture request.'));
        }
        return $response;
    }

    public function limpaCPF_CNPJ($valor){
        $valor = trim($valor);
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", "", $valor);
        $valor = str_replace("-", "", $valor);
        $valor = str_replace("/", "", $valor);
        return $valor;
    }
    
    public function callApi($request){
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('Psr\Log\LoggerInterface');
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        $scopeConfig = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        $env = $scopeConfig->getValue("payment/classyllama_llamacoin/production", $storeScope); 

        if($env == 1){
            $username = $scopeConfig->getValue("payment/classyllama_llamacoin/username", $storeScope);
            $keyValue = $scopeConfig->getValue("payment/classyllama_llamacoin/keyvalue", $storeScope);
            $codigoCanal = $scopeConfig->getValue("payment/classyllama_llamacoin/codigocanal", $storeScope);
            $codigoCliente = $scopeConfig->getValue("payment/classyllama_llamacoin/codigocliente", $storeScope);
            $loginUrl = 'https://pinbank.com.br/services/api/token';
        } else if($env  == 0){
            $username = $scopeConfig->getValue("payment/classyllama_llamacoin/username", $storeScope);
            $keyValue = $scopeConfig->getValue("payment/classyllama_llamacoin/keyvalue", $storeScope);
            $codigoCanal = $scopeConfig->getValue("payment/classyllama_llamacoin/codigocanal", $storeScope);
            $codigoCliente = $scopeConfig->getValue("payment/classyllama_llamacoin/codigocliente", $storeScope);
            $loginUrl = 'https://dev.pinbank.com.br/services/api/token';
        }

        $keyLoja = $scopeConfig->getValue("payment/classyllama_llamacoin/keyloja", $storeScope);

        $quantidadeParcelas= (int)$request['parcelas'];
        $cartaoData = ['Data' => 
            ["CodigoCanal"=> (int)$codigoCanal,
            "CodigoCliente"=> (int)$codigoCliente,
            //"DataValidade"=>$DataValidade,
            "KeyLoja"=>$keyLoja,
            "QuantidadeParcelas" => $quantidadeParcelas,
            "DescricaoPedido"=>"PQL",
            "CpfComprador"=>$this->limpaCPF_CNPJ($request['cpf']),
            "TransacaoPreAutorizada"=>false
            ]
        ];

        if($request['cc_exp_month'] < 10){
            $request['cc_exp_month'] = '0'.$request['cc_exp_month'];
        }
        $cartaoData['Data']['Valor'] = (float)$request['amount']*100;
        $cartaoData['Data']['NumeroCartao'] = $request['cc_number'];
        $cartaoData['Data']['CodigoSeguranca'] = $request['cc_cid'];
        if($quantidadeParcelas > 1){
            $cartaoData['Data']['FormaPagamento'] = '2';
        } else{
            $cartaoData['Data']['FormaPagamento'] = '1';
        }
        $cartaoData['Data']['DataValidade'] = $request['cc_exp_year'].$request['cc_exp_month'];

        $cartaoData['Data']['NomeImpresso'] = $request['nome_impresso'];
        $jsonEnviar = $this->gerarJson($cartaoData, $keyValue);

        $urlEfetuarTransacao = 'https://pinbank.com.br/services/api/Transacoes/EfetuarTransacaoEncrypted';
        $jsonDecoded = $this->curlApi($loginUrl, $username, $keyValue, $jsonEnviar, $urlEfetuarTransacao, $codigoCanal, $codigoCliente);
        return $jsonDecoded;
    }

    public function fnDecrypt($sValue, $sSecretKey){
        
        $key = $sSecretKey;
        $plaintext = $sValue;
        $cipher = "AES-128-CBC";    
        if (in_array($cipher, openssl_get_cipher_methods()))
        {
            $ivlen = openssl_cipher_iv_length($cipher);
            $iv = openssl_random_pseudo_bytes($ivlen);
            $ciphertext = openssl_decrypt($plaintext, $cipher, $key, $options=0, $iv);
            return $ciphertext;
        }
        return false;

        $key = $sSecretKey;
        $plaintext = $sValue;
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_decrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
        $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
        return $ciphertext;
    }
    
    public function fnEncrypt($sValue, $sSecretKey){
        $key = $sSecretKey;
        $plaintext = $sValue;
        $cipher = "AES-128-CBC";
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = substr($data, 0, $iv_size);
        $ciphertext = openssl_encrypt($plaintext, $cipher, $key, $options=0, $iv);
        return $ciphertext;
    }
    
    public function curlApi($loginUrl, $username, $keyValue, $jsonEnviar, $uri, $codigoCanal, $codigoCliente){
        
        $ch = curl_init();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('Psr\Log\LoggerInterface');
        $return = $this->loginApi($loginUrl, $username, $keyValue, $codigoCanal, $codigoCliente);        
        $accessToken = $return['access_token'];
        $tokenType = 'Bearer';
        $requestOrigin = 5;    
        $httpHeader = array(
            "Authorization: ".$tokenType." ". $accessToken,
            "UserName: ".$username,
            "Content-Type: application/json",
            "RequestOrigin: ".$requestOrigin,
        );
        curl_setopt($ch, CURLOPT_URL,$uri);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonEnviar);           
        curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        $jsonDecoded = json_decode($server_output, true);
        $toDecrypt = $jsonDecoded['Data']['Json'];
        $toDecrypted = $this->fnDecrypt($toDecrypt, $keyValue);
        curl_close ($ch);
        return $jsonDecoded;
    }
    
    public function gerarJson($data, $keyValue){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('Psr\Log\LoggerInterface');
        //$crypted = 'j+EF/FGn0QB7Jz321wkuEDOjmdJUBSG86v/Z27wU81rXWUUWouYlVgHVdH1p5hY6GiKu/AkQe2lKQV+yzoEqn08rlUf9WfO7C14iW/Ytd0e6idJCHsjvytcB3yInUh5ecnTZy8NJ8YiDOmUPf6znfmgMsp4+LLndZ1ZlULpKV2kg5kR8Oyeea9007dZCjCL4U0hZemqECAADuttCAuzrEb9BfJFoRWJ0BLdqxHv7Pqn+K6dPsZhjS/yjJhqQ6LYLi9rdI69SLBX2R9A+5VQ87+eMxRDrEIucJqY/hSF4Bi02adH8UnClEYxfAwoPuM4tT1SiAxoQsx16LMVJb0zV3t0nrXzVQRZvEE4KFj1xtH1MDsNxNZEpu4ksD8+njTPWjrcZW6iok/UvZOpOEm85SMXiIk4GZ/HZ7q2V2byJeM4TrFGlIzjgugke/rjiePWQeDTEEdoG3l8IKbGdhUi+CQ==';
        $jsonOriginal = json_encode($data);
        $Pass = $keyValue;
        $Clear = $jsonOriginal;
        $crypted = $this->fnEncrypt($Clear, $Pass);
        $postData = ['Data'=>['Json'=>$crypted]];

        $jsonEnviar = json_encode($postData);
        return $jsonEnviar;
    }
    
    public function loginApi($loginUrl, $username, $keyValue, $codigoCanal, $codigoCliente){
        //Iniciar autenticação
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logger = $objectManager->create('Psr\Log\LoggerInterface');
        $url = $loginUrl;
        $ch = curl_init();
        $requestOrigin = 5;
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"username=".$username."&password=".$keyValue."&grant_type=password");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        $return = json_decode($server_output, true);
        //Terminar autenticação
        return $return;
    }
}