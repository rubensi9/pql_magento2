<?php                                                                 
namespace Tagweb\Catalogoptions\Observer;

class SetPrice implements \Magento\Framework\Event\ObserverInterface          
{
 protected $_request;
 public function __construct(
    \Magento\Framework\App\RequestInterface $request,
    array $data = []
 ) {
    $this->_request = $request;
 }

 public function execute(
    \Magento\Framework\Event\Observer $observer
 ) {
    $postdata = $this->_request->getPost();
    $smartcatalogoptionFinalprice = $postdata['smartcatalogoption_finalprice']; // say it select input field which you have
  
    $item = $observer->getEvent()->getData('quote_item');
    $item = ($item->getParentItem() ? $item->getParentItem() : $item);
    $price = $smartcatalogoptionFinalprice; //set your price here
    $item->setCustomPrice($price);
    $item->setOriginalCustomPrice($price);
    $item->getProduct()->setIsSuperMode(true);
 }
}