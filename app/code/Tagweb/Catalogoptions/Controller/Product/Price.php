<?php /**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Tagweb\Catalogoptions\Controller\Product;
use Magento\Catalog\Model\Product;

class Price extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $formKey;   
    protected $cart;
    protected $product;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(\Magento\Framework\App\Action\Context $context)
    {  
        parent::__construct($context);
    }
    /**
     * View page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productId = $params['productId'];
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
        echo $product->getFinalPrice();
    } 

}
