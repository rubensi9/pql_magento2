<?php
ob_start();
function fnDecrypt($sValue, $sSecretKey){
    $key = $sSecretKey;
    $plaintext = $sValue;
    $cipher = "AES-128-CBC";
    if (in_array($cipher, openssl_get_cipher_methods()))
    {
        $ivlen = openssl_cipher_iv_length($cipher);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = substr($data, 0, $iv_size);
        $ciphertext = openssl_decrypt($plaintext, $cipher, $key, $options=0, $iv);
        return $ciphertext;
    }
    return false;    
}

function fnEncrypt($sValue, $sSecretKey){
    /*$key = $sSecretKey;
    $plaintext = $sValue;
    $cipher = "AES-128-CBC";
    $ivlen = openssl_cipher_iv_length($cipher);
    $iv = openssl_random_pseudo_bytes($ivlen);
    $ciphertext = openssl_encrypt($plaintext, $cipher, $key, $options=0, $iv, $tag);
    echo '<pre>' . var_export($ciphertext, true) . '</pre>';
    return $ciphertext;*/

    
    $key = $sSecretKey;
    $plaintext = $sValue;
    $cipher = "AES-128-CBC";
    $ivlen = openssl_cipher_iv_length($cipher);
    //$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
    $iv = substr($plaintext, 0, 16);
    $ciphertext = openssl_encrypt($plaintext, $cipher, $key, $options=0, $iv);
    return $ciphertext;
}

function curlApi($loginUrl, $username, $keyValue, $jsonEnviar, $uri, $codigoCanal, $codigoCliente){
    $ch = curl_init();
    $return = loginApi($loginUrl, $username, $keyValue, $codigoCanal, $codigoCliente);    
    $accessToken = $return['access_token'];
    //$tokenType = $return['token_type'];
    $tokenType = 'Bearer';
    $requestOrigin = 5;    
    $httpHeader = array(
        "Authorization: ".$tokenType." ". $accessToken,
        "UserName: ".$username,
        "Content-Type: application/json",
        "RequestOrigin: ".$requestOrigin,
    );
    curl_setopt($ch, CURLOPT_URL,$uri);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonEnviar);           
    curl_setopt($ch, CURLOPT_HTTPHEADER, $httpHeader);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    $jsonDecoded = json_decode($server_output, true);
    $toDecrypted = fnDecrypt($toDecrypt, $keyValue);
    $log  = date("d-m-Y h:i:s").PHP_EOL.$toDecrypt.PHP_EOL.$resultCode.PHP_EOL.$message.PHP_EOL."-------------------------".PHP_EOL;
    file_put_contents('./pinbank_logs/pinbank_decrypted_log_'.date("j.n.Y").'.txt', var_dump($toDecrypted), FILE_APPEND);
    curl_close ($ch);
    return $jsonDecoded;
}

function gerarJson($data, $keyValue){
    $jsonOriginal = json_encode($data);
    $Pass = $keyValue;
    $Clear = $jsonOriginal;
    $crypted = fnEncrypt($Clear, $Pass);
    //$crypted = 'j+EF/FGn0QB7Jz321wkuEDOjmdJUBSG86v/Z27wU81rXWUUWouYlVgHVdH1p5hY6GiKu/AkQe2lKQV+yzoEqn08rlUf9WfO7C14iW/Ytd0e6idJCHsjvytcB3yInUh5ecnTZy8NJ8YiDOmUPf6znfmgMsp4+LLndZ1ZlULpKV2kg5kR8Oyeea9007dZCjCL4U0hZemqECAADuttCAuzrEb9BfJFoRWJ0BLdqxHv7Pqn+K6dPsZhjS/yjJhqQ6LYLi9rdI69SLBX2R9A+5VQ87+eMxRDrEIucJqY/hSF4Bi02adH8UnClEYxfAwoPuM4tT1SiAxoQsx16LMVJb0zV3t0nrXzVQRZvEE4KFj1xtH1MDsNxNZEpu4ksD8+njTPWjrcZW6iok/UvZOpOEm85SMXiIk4GZ/HZ7q2V2byJeM4TrFGlIzjgugke/rjiePWQeDTEEdoG3l8IKbGdhUi+CQ==';
    $postData = ['Data'=>['Json'=>$crypted]];
    $jsonEnviar = json_encode($postData);
    return $jsonEnviar;
}

function loginApi($loginUrl, $username, $keyValue, $codigoCanal, $codigoCliente){
    $url = $loginUrl;
    $ch = curl_init();
    $requestOrigin = 5;
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,"username=".$username."&password=".$keyValue."&grant_type=password");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $server_output = curl_exec ($ch);
    $return = json_decode($server_output, true);
    //Terminar autenticação
    return $return;
}

$env = $_GET['env'];
if($env == null){
    $env = 'dev';
}

if($env  == 'prod'){
    $username = 'EgOYU63j5XpN';
    $keyValue = 'v2yBPFsOzfJhV1Ad';
    $codigoCanal = 401;
    $codigoCliente = 129317;
    $loginUrl = 'https://pinbank.com.br/services/api/token';
} else if($env  == 'dev'){
    $username = 'EgOYU63j5XpN';
    $keyValue = 'v2yBPFsOzfJhV1Ad';
    $codigoCanal = 47;
    $codigoCliente = 3510;
    $loginUrl = 'https://dev.pinbank.com.br/services/api/token';
}

$keyLoja = '11384322623341877660';

$params = $_POST;

$ano = (int)$params['ano'];
if($ano < 100 && $ano > 9){
    $ano += 2000;
}
$DataValidade = $ano.$params['mes'];
$quantidadeParcelas= 1;
$cartaoData = ['Data' => ["CodigoCanal"=>$codigoCanal,"CodigoCliente"=>$codigoCliente,"DataValidade"=>$DataValidade,"KeyLoja"=>$keyLoja,"QuantidadeParcelas" => $quantidadeParcelas,"DescricaoPedido"=>"PQL","CpfComprador"=>$params['CpfComprador'],"TransacaoPreAutorizada"=>false]];

foreach($params as $paramkey => $paramvalue){
    if($paramkey == 'Valor'){
        $paramvalue = (int)$paramvalue*100;
        $paramvalue = (float)$paramvalue;
    }
    if($paramkey == 'QuantidadeParcelas'){
        $paramvalue = (int)$paramvalue;
        $paramvalue = (float)$paramvalue;
    }
    if($paramkey !== 'mes' && $paramkey !== 'ano'){
        $cartaoData['Data'][$paramkey] = $paramvalue;
    }
}
$jsonEnviar = gerarJson($cartaoData, $keyValue);
echo '<pre>' . var_export($jsonEnviar, true) . '</pre>';

$urlEfetuarTransacao = 'https://dev.pinbank.com.br/services/api/Transacoes/EfetuarTransacaoEncrypted';
$jsonDecoded = curlApi($loginUrl, $username, $keyValue, $jsonEnviar, $urlEfetuarTransacao, $codigoCanal, $codigoCliente);
echo '<pre>' . var_export($jsonDecoded, true) . '</pre>';
$resultCode = $jsonDecoded['ResultCode'];

if($resultCode == 0){
    exit();
} else{
    $message = $jsonDecoded['Message'];
    $toDecrypt = $jsonDecoded['Data']['Json'];
    $log  = date("d-m-Y h:i:s").PHP_EOL.$toDecrypt.PHP_EOL.$resultCode.PHP_EOL.$message.PHP_EOL."-------------------------".PHP_EOL;
    file_put_contents('./pinbank_logs/pinbank_error_log_'.date("j.n.Y").'.txt', $log, FILE_APPEND);
    exit();
}